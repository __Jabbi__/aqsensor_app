package aqsensor.de.aq_sensor;

/**
 * Created by felix on 10.01.17.
 */
public enum ProgressWheelState {
    good,
    middle,
    bad
}
