package aqsensor.de.aq_sensor;

import android.app.ProgressDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.Intent;
import android.database.sqlite.SQLiteBlobTooBigException;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;

import android.widget.ListView;
import android.widget.Toast;


import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Set;

public class LoginActivity extends AppCompatActivity {
    private final static int REQUEST_ENABLE_BT = 1;
    private Set<BluetoothDevice> pairedDevices;
    private Object[] bdArray; //ONLY FOR BLUETHOOTHDEVICE!!!
    public Button b1,b3;
    public BluetoothAdapter  BA;
    public ListView lv;
    public static String EXTRA_ADRESS = "test";
    ProgressDialog progress;
    public boolean isBtConnected;
    public BluetoothSocket btSocket = null;

    public String address;
    private AdapterView.OnItemClickListener myListClickListener = new AdapterView.OnItemClickListener()
    {
        public void onItemClick (AdapterView av, View v, int arg2, long arg3)
        {
            address = ( (BluetoothDevice) bdArray[arg2]).getAddress();
            new ConnectBT().execute();
        }
    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        /**
        ProgressWheel pw = (ProgressWheel) findViewLoginActivityById(R.id.pw_LoginSpinner);
        pw.setBarColor(Color.GREEN);
        pw.startSpinning();*/
        setTitle("Sensor auswählen");
        lv = (ListView)findViewById(R.id.ListView);
        BA = BluetoothAdapter.getDefaultAdapter();
        b1 = (Button) findViewById(R.id.button);
        b3=(Button)findViewById(R.id.button3);
        Button testButon = (Button) findViewById(R.id.ts);
        testButon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(LoginActivity.this, ControllActivity.class);
                startActivity(i);
            }
        });
        Button aboutButton = (Button) findViewById(R.id.about);
        aboutButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(LoginActivity.this, AboutPage.class);
                startActivity(i);
            }
        });
        list();
        on();
    }

    public void on(View v)
    {
       on();
    }
    public void on(){
        if (!BA.isEnabled()) {
            Intent turnOn = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(turnOn, 0);
            Toast.makeText(getApplicationContext(), "Turned on",Toast.LENGTH_LONG).show();
        } else {
            Toast.makeText(getApplicationContext(), "Already on", Toast.LENGTH_LONG).show();
        }
    }
    public  void visible(View v){
        Intent getVisible = new Intent(BluetoothAdapter.ACTION_REQUEST_DISCOVERABLE);
        startActivityForResult(getVisible, 0);
    }
    public void list(View v){
       list();
    }
    public void list()
    {
        pairedDevices = BA.getBondedDevices();
        bdArray = pairedDevices.toArray();
        ArrayList list = new ArrayList();

        for(BluetoothDevice bt : pairedDevices) list.add(bt.getName());
        Toast.makeText(getApplicationContext(), "Showing Paired Devices",Toast.LENGTH_SHORT).show();

        final ArrayAdapter adapter = new  ArrayAdapter(this,android.R.layout.simple_list_item_1, list);

        lv.setAdapter(adapter);
        lv.setOnItemClickListener(myListClickListener);
    }
    public static BluetoothSocket btSocketCreate(String Sadress) throws NoSuchMethodException, IllegalAccessException, InvocationTargetException{
        BluetoothAdapter myBluetooth = BluetoothAdapter.getDefaultAdapter();//get the mobile bluetooth device
        BluetoothDevice dispositivo = myBluetooth.getRemoteDevice(Sadress);//connects to the device's address and checks if it's available
        Log.e("LoginActivity","Get RfcommToserviceRexord");
        Method m = dispositivo.getClass().getMethod("createInsecureRfcommSocket", new Class[] {int.class});
        BluetoothAdapter.getDefaultAdapter().cancelDiscovery();
        Log.e("LoginActivity","Invoke Method");
        BluetoothSocket SbtSocket = (BluetoothSocket) m.invoke(dispositivo,3);
        Log.e("LoginActivity","connection start");
        return SbtSocket;
    }
    public static passClass btSocketConnect(String Saddress){
        boolean SConnectSuccess = true;
        BluetoothSocket SbtSocket = null;
        try
        {
                SbtSocket = btSocketCreate(Saddress);
                SbtSocket.connect();//start connection

        }
        catch (IOException e)
        {
            Log.e("LoginActivity", e.toString());
            SConnectSuccess = false;
        }
        catch (NoSuchMethodException e){
            Log.e("LoginActivity", e.toString());
            SConnectSuccess = false;

        }
        catch (java.lang.IllegalAccessException e){
            Log.e("LoginActivity", e.toString());
            SConnectSuccess = false;


        }
        catch (java.lang.reflect.InvocationTargetException e)
        {
            Log.e("LoginActivity", e.toString());
            SConnectSuccess = false;

        }
        passClass pC = new passClass();
        pC.btSocket = SbtSocket;
        pC.ConnectedSucces = SConnectSuccess;
        return pC;
    }
    private class ConnectBT extends AsyncTask<Void, Void, Void>  // UI thread
    {
        private boolean ConnectSuccess = true; //if it's here, it's almost connected

        @Override
        protected void onPreExecute()
        {
            progress = ProgressDialog.show(LoginActivity.this, "Connecting...", "Please wait!!!");  //show a progress dialog
        }

        @Override
        protected Void doInBackground(Void... devices) //while the progress dialog is shown, the connection is done in background
        {
            if (btSocket == null || !isBtConnected) {
                passClass pC = btSocketConnect(address);
                btSocket = pC.btSocket;
                ConnectSuccess = pC.ConnectedSucces;
            }

            return null;
        }
        @Override
        protected void onPostExecute(Void result) //after the doInBackground, it checks if everything went fine
        {
            super.onPostExecute(result);

            if (!ConnectSuccess)
            {
                Toast.makeText(getApplicationContext(), "Hast du den Sensor ausgewählt? Nochmal probieren.", Toast.LENGTH_LONG).show();
            }
            else
            {
                Toast.makeText(getApplicationContext(), "Verbunden!", Toast.LENGTH_SHORT).show();
                isBtConnected = true;

                Intent intent = new Intent(LoginActivity.this, ControllActivity.class);
                data.btSocket = btSocket;
                startActivity(intent);
            }
            progress.dismiss();
        }
    }


}
